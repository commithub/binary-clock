# binary-clock

[![Netlify Status](https://api.netlify.com/api/v1/badges/0784054e-451b-4255-a67a-f2ba8136c1cf/deploy-status)](https://app.netlify.com/sites/binary-clock-app/deploys)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
